const express = require('express');
const router = express.Router();
//const { ensureAuthenticated, forwardAuthenticated } = require('../config/auth');
const loginController = require('../controller/login')
const registerController = require('../controller/register')

router.get('/users/register', loginController.registerGet);
router.get('/', loginController.loginGet);
router.post('/users/register', registerController.registerNewUser);
router.post('/dashboard', loginController.loginUser);

module.exports = router;
