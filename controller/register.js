const express = require('express');
const router = express.Router();
const sqlCon = require('../connection/con');

exports.registerNewUser = function(req, response){
    var newUser = {
        username: req.body.username, 
        email: req.body.email,
        mobile: req.body.mobile,
        password: req.body.password,
    }
    console.log(newUser);
    if (newUser.username && newUser.password) {
		sqlCon.query('INSERT INTO Users (username,email,mobile,password) Values ?', [newUser.username, newUser.email, newUser.mobile, newUser.password], function(error, results, fields) {
            console.log('test');
            response.redirect('/');
			response.end();
		});
	} else {
		response.send('Please enter Username and Password!');
		response.end();
	}
}
